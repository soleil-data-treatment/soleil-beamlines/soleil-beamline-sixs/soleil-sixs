#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 14:39:36 2019
Meant to open the data generated from the datarecorder upgrade of january 2019
Modified again the 24/06/2020
@author: andrea
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
#from __future__ import unicode_literals
import tables
import os
import numpy as np
import pickle
import time
from matplotlib import pyplot as plt

class emptyO(object):
    '''Empty class used as container in the nxs2spec case. '''
    pass

class DataSet(object):
    '''Dataset read the file and store it in an object, from this object we can 
    retrive the data to use it.

    Use as:
         dataObject = nxsRead3.DataSet( path/filename, path )
         filename can be '/path/filename.nxs' or just 'filename.nxs'
         directory is optional and it can contain '/dir00/dir01'
         both are meant to be string.
    
        . It returns an object with attibutes all the recorded sensor/motors 
        listed in the nxs file.
        . if the sensor/motor is not in the "aliad_dict" file it generate a 
        name from the device name.
    
    The object will also contains some basic methods for data reuction such as 
    ROI extraction, attenuation correction and plotting.
    Meant to be used on the data produced after the 11/03/2019 data of the 
    upgrade of the datarecorder''' 
    def __init__(self,filename, directory = '', Nxs2Spec = False):
        
        self.directory = directory 
        self.filename = filename
        self.end_time = 2
        self.start_time = 1
        self._attlist = []
        attlist = []  # used for self generated file attribute list 
        aliases = [] # used for SBS imported with alias_dict file
        
        try:
            self._alias_dict = pickle.load(open('/home/andrea/MyPy3/alias_dict.txt','rb'))
            #self._alias_dict = pickle.load(open('/home/experiences/sixs/com-sixs/python/alias_dict.txt','rb'))
        except :#FileNotFoundError:
            print('NO ALIAS FILE')
            self._alias_dict = None
            
        def is_empty(any_structure):
            '''Quick function to determine if an array, tuple or string is 
            empty '''
            if any_structure:
                return False
            else:
                return True
        ## Load the file 
        fullpath = os.path.join(self.directory,self.filename)
        ff = tables.open_file(fullpath,'r') 
        f = ff.list_nodes('/')[0]
        ################  check if any scanned data a are present  
        try:
            if not hasattr(f, 'scan_data'):
                self.scantype = 'unknown'
                ff.close()
                return
        except:
            return
    
        
        #### Discriminating between SBS or FLY scans
        
        try:
            if f.scan_data.data_01.name == 'data_01':
                self.scantype = 'SBS'
        except tables.NoSuchNodeError:
            self.scantype = 'FLY'
            
        
        ########################## Reading FLY ################################        
        if self.scantype == 'FLY':
            ### generating the attributes with the recorded scanned data
            
            for leaf in f.scan_data:
                list.append(attlist,leaf.name) 
                self.__dict__[leaf.name] = leaf[:]
                time.sleep(0.1)
            self._attlist = attlist 
            
        ###################### Reading SBS ####################################
        if self.scantype == 'SBS':
            if  self._alias_dict:  #### Reading with dictionary
                for leaf in f.scan_data:
                        try :
                            alias = self._alias_dict[leaf.attrs.long_name.decode('UTF-8')]
                            if alias not in aliases:
                                aliases.append(alias)
                                self.__dict__[alias]=leaf[:]
                            
                        except :
                            self.__dict__[leaf.attrs.long_name.decode('UTF-8')]=leaf[:]
                            aliases.append(leaf.attrs.long_name.decode('UTF-8'))
                            pass
                self._attlist = aliases
            
            else:
                for leaf in f.scan_data: #### Reading with dictionary
                    ### generating the attributes with the recorded scanned data    
                    attr = leaf.attrs.long_name.decode('UTF-8')
                    attrshort = leaf.attrs.long_name.decode('UTF-8').split('/')[-1]
                    attrlong = leaf.attrs.long_name.decode('UTF-8').split('/')[-2:]
                    if attrshort not in attlist:
                        if attr.split('/')[-1] == 'sensorsTimestamps':   ### rename the sensortimestamps as epoch
                            list.append(attlist, 'epoch')
                            self.__dict__['epoch'] = leaf[:]
                        else:
                            list.append(attlist,attr.split('/')[-1])
                            self.__dict__[attr.split('/')[-1]] = leaf[:]
                    else: ### Dealing with for double naming
                        list.append(attlist, '_'.join(attrlong))
                        self.__dict__['_'.join(attrlong)] = leaf[:]
                self._attlist = attlist   
       
        #########   this is for nxs2spec transformations##################################################################################
        ##################################################################################################################################
        if Nxs2Spec:
            self._nxs2spec = emptyO()
            HKL_pre = False
            print('Nxs2Spec', self.filename)
            try: # try reading for h k l coordinates at the beginning of the scan. even without hkl transformer  UHV used in nxs2spec
                h_tmp = f.sample_info._f_get_child('i14-c-cx2-ex-diff-uhv-h')._f_get_child('position_pre').read()[0]
                k_tmp = f.sample_info._f_get_child('i14-c-cx2-ex-diff-uhv-k')._f_get_child('position_pre').read()[0]  # ok 
                l_tmp = f.sample_info._f_get_child('i14-c-cx2-ex-diff-uhv-l')._f_get_child('position_pre').read()[0]
                #self.hkl_string = '#Q ' + str(h_tmp) + ' ' + str(k_tmp) + ' ' + str(l_tmp) + '\n'
                self._nxs2spec.hkl_string = ('#Q ' + str(h_tmp) + ' ' + str(k_tmp) + ' ' + str(l_tmp) + '\n')
                #print(self._nxs2spec.hkl_string) 
                self._setup =  'UHV'
                HKL_pre = True
            except:
                pass
           
            try: # try reading for h k l coordinates at the beginning of the scan. even without hkl transformer  MED_h  used in nxs2spec
                h_tmp = f.sample_info._f_get_child('i14-c-cx1-ex-dif-med.h-h')._f_get_child('position_pre').read()[0]
                k_tmp = f.sample_info._f_get_child('i14-c-cx1-ex-dif-med.h-k')._f_get_child('position_pre').read()[0]
                l_tmp = f.sample_info._f_get_child('i14-c-cx1-ex-dif-med.h-l')._f_get_child('position_pre').read()[0]
                self._nxs2spec.hkl_string = ('#Q ' + str(h_tmp) + ' ' + str(k_tmp) + ' ' + str(l_tmp) + '\n')
                self._setup =  'MED_H'
                HKL_pre = True
            except:
               pass
                
            try: # try reading for h k l coordinates at the beginning of the scan. even without hkl transformer  MED_h  used in nxs2spec
                h_tmp = f.sample_info._f_get_child('i14-c-cx1-ex-dif-med.1-h')._f_get_child('position_pre').read()[0]
                k_tmp = f.sample_info._f_get_child('i14-c-cx1-ex-dif-med.1-k')._f_get_child('position_pre').read()[0]
                l_tmp = f.sample_info._f_get_child('i14-c-cx1-ex-dif-med.1-l')._f_get_child('position_pre').read()[0]
                self._nxs2spec.hkl_string = ('#Q ' + str(h_tmp) + ' ' + str(k_tmp) + ' ' + str(l_tmp) + '\n')
                self._setup =  'MED'
                HKL_pre = True
            except:
                pass
                
            if not HKL_pre: ## communicate it to user
                print('No HKL_prescan')
                ############ building the P lines for the nxs2spec ########
            #if self._setup ==  'UHV':
            P0 = False
            try:
                mu = f.SIXS._f_get_child('i14-c-cx2-ex-mu-uhv')._f_get_child('position_pre').read()[0]
                omega = f.SIXS._f_get_child('i14-c-cx2-ex-omega-uhv')._f_get_child('position_pre').read()[0]
                delta = f.SIXS._f_get_child('i14-c-cx2-ex-delta-uhv')._f_get_child('position_pre').read()[0]
                gamma = f.SIXS._f_get_child('i14-c-cx2-ex-gamma-uhv')._f_get_child('position_pre').read()[0]
                p1 = str(delta)
                p2 = str(omega)
                p3 = str(0.000)
                p4 = str(0.000)
                p5 = str(mu)
                p6 = str(gamma)
                p7 = str(gamma)
                self._nxs2spec.P_line = ('#P0 ' + p1 + ' ' + p2 + ' '+ p3 + ' '+ p4 + ' '+ p5 + ' ' + p6 +' ' + p7 + '\n')
                P0 =  True
            except :
                pass
            #if self._setup ==  'MED_H':
            try:
                mu = f.SIXS._f_get_child('i14-c-cx1-ex-mu-med-h')._f_get_child('position_pre').read()[0]
                delta = f.SIXS._f_get_child('i14-c-cx1-ex-delta-med-h')._f_get_child('position_pre').read()[0]
                gamma = f.SIXS._f_get_child('i14-c-cx1-ex-gamma-med-h')._f_get_child('position_pre').read()[0]
                beta = f.SIXS._f_get_child('i14-c-cx1-ex-diff-med-tpp')._f_get_child('position_pre').read()[0]
                p1 = str(gamma)
                p2 = str(mu)
                p3 = str(0.000)
                p4 = str(0.000)
                p5 = str(beta)
                p6 = str(delta)
                p7 = str(delta)
                self._nxs2spec.P_line = ('#P0 ' + p1 + ' ' + p2 + ' '+ p3 + ' '+ p4 + ' '+ p5 + ' ' + p6 +' ' + p7 + '\n')
                P0 =  True
            except :
                pass
            #if self._setup ==  'MED':
            try:
                mu = f.SIXS._f_get_child('i14-c-cx1-ex-mu-med-v')._f_get_child('position_pre').read()[0]
                delta = f.SIXS._f_get_child('i14-c-cx1-ex-delta-med-v')._f_get_child('position_pre').read()[0]
                gamma = f.SIXS._f_get_child('i14-c-cx1-ex-gamma-med-v')._f_get_child('position_pre').read()[0]
                omega = f.SIXS._f_get_child('i14-c-cx1-ex-omega-med-v')._f_get_child('position_pre').read()[0]
                p1 = str(delta)
                p2 = str(omega)
                p3 = str(0.000)
                p4 = str(0.000)
                p5 = str(mu)
                p6 = str(gamma)
                p7 = str(gamma)
                self._nxs2spec.P_line = ('#P0 ' + p1 + ' ' + p2 + ' '+ p3 + ' '+ p4 + ' '+ p5 + ' ' + p6 +' ' + p7 + '\n')
                P0 =  True
            except :
                pass
            if not P0:
                print('No P0 Line')
         #####################################################################################################################   
      
       
         ### adding some useful attributes common between SBS and FLY#########################################################
        try:
            mono = f.SIXS.__getattr__('i14-c-c02-op-mono')
            self.waveL = mono.__getattr__('lambda')[0]
            self.energymono = mono.energy[0]
        except (tables.NoSuchNodeError):
            self.energymono = f.SIXS.Monochromator.energy[0]
            self.waveL = f.SIXS.Monochromator.wavelength[0]
        #### probing time stamps and eventually use epoch to rebuild them
        if hasattr(f, 'end_time'): # sometimes this attribute is absent, especially on the ctrl+C scans
            try:
                self.end_time = f.end_time._get_obj_timestamps().ctime
            except:
                if is_empty(np.shape(f.end_time)):
                    try:
                        self.end_time = max(self.epoch)
                    except AttributeError:
                        self.end_time = 2
                        print('File has time stamps issues')
                else:
                    self.end_time = f.end_time[0]
        elif not hasattr(f, 'end_time'):
            print('File has time stamps issues')
            self.end_time = 2 #necessary for nxs2spec conversion
            
        if hasattr(f, 'start_time'):    
            try:
                self.start_time = f.start_time._get_obj_timestamps().ctime
            except:
                if is_empty(np.shape(f.start_time)):
                    try:
                        self.start_time = min(self.epoch)
                    except AttributeError:
                        self.start_time = 1
                        print('File has time stamps issues')
                else:
                    self.start_time = f.start_time[0]
        elif not hasattr(f, 'start_time'): # sometimes this attribute is absent, especially on the ctrl+C scans
            print('File has time stamps issues')
            self.start_time = 1 #necessary for nxs2spec conversion
        try:   ######## att_coef
            self._coef =  f.SIXS._f_get_child('i14-c-c00-ex-config-att').att_coef[0]
        except:
            print('No att coef')
        try:
            self._integration_time  =  f.SIXS._f_get_child('i14-c-c00-ex-config-publisher').integration_time[0]
        except:
            self._integration_time = 1            
            print('No integration time defined')
        try:
            self._roi_limits = f.SIXS._f_get_child('i14-c-c00-ex-config-publisher').roi_limits[:][:]
            #self.roi_names = str(f.SIXS._f_get_child('i14-c-c00-ex-config-publisher').roi_name.read()).split()
            roi_names_cell = f.SIXS._f_get_child('i14-c-c00-ex-config-publisher').roi_name.read()
            self._roi_names = roi_names_cell.tolist().decode().split('\n')
        except:
            print('No ROI defined')
        try:
            self.mask  =  f.SIXS._f_get_child('i14-c-c00-ex-config-publisher').mask[:]
        except:
            print('No Mask')
            
        ff.close()
    ########################################################################################
    ##################### down here useful function in the NxsRead #########################
    def getStack(self, Det2D_name):
        '''For a given  2D detector name given as string it check in the 
        attribute-list and return a stack of images'''
        try:
            stack = self.__getattribute__(Det2D_name)
            return stack
        except:
            print('There is no such attribute')
    
    def make_maskFrame_xpad(self):
        '''It generate a new attribute 'mask0_xpad' to remove the double pixels
        it can be applied only to xpads140 for now.'''
#    f = tables.open_file(filename)
#    scan_data = f.list_nodes('/')[0].scan_data
        mask = np.zeros((240,560), dtype='bool')
#    imgs = scan_data.data_01
#    for img in imgs:
#        m = numpy.where(img > 2, True, False)
#        mask = numpy.logical_or(mask, m)
#    print('pixels morts=', mask.sum())
        
        mask[:, 79:560:80] = True
        mask[:, 80:561:80] = True
        mask[119, :] = True
        mask[120, :] = True
        mask[:, 559] = False
        self.mask0_xpad = mask
        return   
        
    def roi_sum(self, stack,roi):
        '''given a stack of images it returns the integals over the ROI  
        roi is expected as eg: [257, 126,  40,  40] '''
        return stack[:,roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2]].sum(axis=1).sum(axis=1).copy()

    def roi_sum_mask(self, stack,roi,mask):
        '''given a stack of images it returns the integals over the ROI minus 
        the masked pixels  '''
        _stack = stack[:]*(1-mask.astype('uint16'))
        return _stack[:,roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2]].sum(axis=1).sum(axis=1)
    
    def calcROI(self, stack,roiextent, attcoef, filters, acqTime, ROIname):
        '''To calculate the roi corrected by attcoef, mask, filters, 
        acquisition_time ROIname is the name of the attribute that will be attached to the dataset object
        mind that there might be a shift between motors and filters in the SBS scans'''
        if hasattr(self, 'mask'):
            integrals = self.roi_sum_mask( stack,roiextent,self.mask)
        if not hasattr(self,'mask'):
            integrals = self.roi_sum( stack,roiextent)        

        if self.scantype == 'SBS':   # here handling the data shift between data and filters SBS
            _filterchanges = np.where((filters[1:]-filters[:-1])!=0)
            roiC = (integrals[:]*(attcoef**filters[:]))/acqTime
            _filterchanges = np.asanyarray(_filterchanges)
            np.put(roiC, _filterchanges+1, np.NaN)
            setattr(self, ROIname, roiC)
            self._attlist.append(ROIname)
        if self.scantype == 'FLY':
            f_shi = np.concatenate((filters[2:],filters[-1:],filters[-1:]))    # here handling the data shift between data and filters FLY
            roiC = (integrals[:]*(attcoef**f_shi[:]))/acqTime
            setattr(self, ROIname, roiC)
            self._attlist.append(ROIname)
        return

    def plotRoi(self, motor, roi,color='-og', detname = None,Label=None, mask = 'No'):
        '''It integrates the desired roi and plot it
        this plot function is simply meant as quick verification.
            Motor: motor name string
            roi: is the roi name string of the desired region measured or in the form :[257, 126,  40,  40]
            detname: detector name;  it used first detector it finds if not differently specified  '''
        if not detname:
            detname = self.det2d()[0]
            print(detname)
            
        if motor in self._attlist:
            xmot = getattr(self, motor)
        if detname:
            #detname = self.det2d()[0]
            stack = self.getStack(detname)
            if isinstance(roi, str):
                roiArr = self._roi_limits[self._roi_names.index(roi)]
            if isinstance(roi, list):
                roiArr = roi
            yint = self.roi_sum(stack, roiArr)
        #print(np.shape(xmot), np.shape(yint))   
        plt.plot(xmot, yint, color, label=Label)
        
    def plotscan(self, Xvar, Yvar,color='-og', Label=None, mask = 'No'):
        '''It plots Xvar vs Yvar.
        Xvar and Yvar must be in the attributes list'''
        if Xvar in self._attlist:
            x = getattr(self, Xvar)
            print('x ok')
        if Yvar in self._attlist:
            y = getattr(self, Yvar)
            print('y ok')
        plt.plot(x, y, color, label=Label)
        
               
    
    def calcROI_new(self):
        '''if exist _coef, _integration_time, _roi_limits, _roi_names it can be applied
        to recalculate the roi
        filters and motors are shifted of one points for the FLY. corrected in the self.calcROI
        For SBS the data point when the filter is changed is collected with no constant absorber and therefore is rejected.'''
        if self.scantype == 'SBS': ############################ SBS Correction #######################################
            possible = hasattr(self, 'xpad') and hasattr(self, '_integration_time') and hasattr(self, 'att_sbs_xpad') \
                        and hasattr(self, '_coef') and hasattr(self, '_roi_limits')and hasattr(self, '_roi_names' ) 
            if not possible:
                print('No correction applied: missing  some data, check for xpad, attenuation, integration time, att_coef and ROIs.')
            if possible:
#                if hasattr(self, '_npts'): 
#                    print('Correction already applied')
                if not hasattr(self, '_npts'): ## check if the process was alredy runned once on this object
                    self._npts = len(self.xpad)
                for pos, roi in enumerate(self._roi_limits, start=0):
                    roiname = self._roi_names[pos]+'c_new'
                    stack = self.xpad[:]
                    attenuators = self.att_sbs_xpad[:]
                    self.calcROI(stack, roi,self._coef, attenuators, self._integration_time, roiname)
                
        if self.scantype == 'FLY':  ######################### FLY correction ##################################
            possible = hasattr(self, 'xpad_image') and hasattr(self, 'attenuation') and hasattr(self, '_integration_time') \
                       and hasattr(self, '_coef') and hasattr(self, '_roi_limits')and hasattr(self, '_roi_names' ) 
            if not possible:
                print('No correction applied: missing  some data, check for xpad, attenuation, integration time, att_coef and ROIs.')   
            
            if possible:
                #print('YES')
                if hasattr(self, '_npts'): 
                    print('Correction already applied')
                if not hasattr(self, '_npts'): ## check if the process was alredy runned once on this object
                    self._npts = len(self.xpad_image)
                    #self._filterchanges = np.where((self.attenuation[1:]-self.attenuation[:-1])!=0)
                    for pos, roi in enumerate(self._roi_limits, start=0):
                        roiname = self._roi_names[pos]+'c_new'
                        stack = self.xpad_image[:]
                        attenuators = self.attenuation[:]  #filters and motors are shifted of one points
                        self.calcROI(stack, roi,self._coef, attenuators, self._integration_time, roiname)
        return
                
                
    def prj(self, axe=0, mask_extra = None):
          '''Project the 2D detector on the coosen axe of the detector and return a matrix 
          of size:'side detector pixels' x 'number of images' 
          axe = 0 ==> x axe detector image
          axe = 1 ==> y axe detector image
          specify a mask_extra variable if you like. 
          Mask extra must be a the result of np.load(YourMask.npy)'''
          if hasattr(self, 'mask'):
              mask = self.__getattribute__('mask')
          if not hasattr(self, 'mask'):
              mask = 1
          if np.shape(mask_extra):
              mask = mask_extra
              if np.shape(mask) == (240,560):
                 self.make_maskFrame_xpad()
                 mask= mask #& self.mask0_xpad
          for el in self._attlist:
              bla = self.__getattribute__(el)
              # get the attributes from list one by one
              if len(bla.shape) == 3: # check for image stacks Does Not work if you have more than one 2D detectors
                  mat = []
                  if np.shape(mask) != np.shape(bla[0]): # verify mask size
                      print(np.shape(mask), 'different from ', np.shape(bla[0]) ,' verify mask size')
                      mask=1
                  for img in bla:
                      if np.shape(mat)[0] == 0:# fill the first line element
                          mat = np.sum(img^mask, axis = axe)
                      if np.shape(mat)[0] > 0:    
                          mat = np.c_[mat, np.sum(img^mask,axis = axe)]
                  setattr(self, str(el+'_prjX'),mat) #generate the new attribute
                  
    def det2d(self):
        '''it retunrs the name/s of the 2D detector'''
        list2D = []
        for el in self._attlist:
              bla = self.__getattribute__(el)
              #print(el, bla.shape)
              # get the attributes from list one by one
              if len(bla.shape) == 3: # check for image stacks
                  list2D.append(el)
        if len(list2D)>0:
            return list2D
        else:
            return False
                          
    
              
          
                








    
    





















                
                
                
                
                
                
                
                
                
                
                
               
