# Aide et ressources de Scripts SIXS pour Synchrotron SOLEIL

## Résumé
- Outils développées par les scientifiques et utilisateurs. Visu rapide, ROI, correction d'intensité. XPADplot, XRDviewer ...
- Fait maison

## Navigation rapide
| Wiki SOLEIL |
| ------ |
| [Tutoriaux](https://gitlab.com/soleil-data-treatment/soleil-sixs/-/wikis/home) |

## Sources
- Code source: XRDviewer deja sur Gitlab
- Documentation officielle: Non, sauf XRDviewer

## Installation
- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien), simple

## Format de données
- en entrée: NXS, variable...
- en sortie: variable
- sur un disque dur, Ruche locale